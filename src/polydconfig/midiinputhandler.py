"""MIDI input handler."""

import logging
import queue

__all__ = ("MidiInputHandler",)
log = logging.getLogger(__name__)


def match_event(event, spec):
    data = {}
    field_lengths = [(x[1] if isinstance(x, (list, tuple)) else 1) for x in spec]

    if "*" not in field_lengths:
        expected_length = sum(field_lengths)

        if len(event) < expected_length:
            return False

    offset = 0
    for item in spec:
        if isinstance(item, str):
            data[item] = event[offset]
            offset += 1
        elif isinstance(item, (list, tuple)):
            name, length = item

            if length == "*":
                data[name] = tuple(event[offset:-1])
                offset = len(event) - 1
            else:
                data[name] = tuple(event[offset : offset + length])  # noqa: E203
                offset += length
        elif item != event[offset]:
            return False
        else:
            offset += 1

    return data


class MidiInputHandler:
    def __init__(self, midiin, name, timeout=3.0):
        self._midiin = midiin
        self._midiin_name = name
        self._timeout = timeout
        self._inq = queue.Queue()
        self._midiin.ignore_types(sysex=False)
        self._midiin.set_callback(self._midiin_callback)

    def flush(self):
        """Flush input queue events."""
        try:
            while True:
                self._inq.get_nowait()
        except queue.Empty:
            pass

    def _midiin_callback(self, event, data):
        msg, dt = event
        log.debug("RECV: %s", " ".join(f"{x:02X}" for x in msg))
        self._inq.put(msg)

    def expect(self, match, *, timeout=None):
        if not self._midiin:
            raise IOError("MIDI input not opened.")

        while True:
            try:
                event = self._inq.get(timeout=timeout if timeout is not None else self._timeout)
            except queue.Empty:
                raise IOError(
                    f"Timout waiting for expected response on MIDI input '{self._midiin_name}'."
                )

            if event:
                log.debug(f"Input queue event: {event!r}")
                if (data := match_event(event, match)) is not False:
                    return data
