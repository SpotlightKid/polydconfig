"""Synthesizer parameter and SysEx command definitions."""

from enum import IntEnum


# Parameter value enums


class VelocityCurve(IntEnum):
    SOFT = 0
    MED = 2
    HARD = 2


class KeyPriority(IntEnum):
    LOW = 0
    HIGH = 1
    LAST = 2


class ModulationCurve(IntEnum):
    SOFT = 0
    MED = 1
    HARD = 2


class ClockMidiOutput(IntEnum):
    OFF = 0
    DIN = 1
    USB = 2
    BOTH = 3


class ExtClockPolarity(IntEnum):
    FALLING = 0
    RISING = 1


class SyncClockRate(IntEnum):
    PPQ_1 = 0
    PPQ_2 = 1
    PPQ_24 = 2
    PPQ_48 = 3


class SyncClockSource(IntEnum):
    INTERNAL = 0
    DIN = 1
    USB = 2
    TRIG = 3


class MWMidiOutput(IntEnum):
    OFF = 0
    DIN = 1
    USB = 2
    BOTH = 3


class PBMidiOutput(IntEnum):
    OFF = 0
    DIN = 1
    USB = 2
    BOTH = 3


class KbdMidiOutput(IntEnum):
    OFF = 0
    DIN = 1
    USB = 2
    BOTH = 3


class ATMidiOutput(IntEnum):
    OFF = 0
    DIN = 1
    USB = 2
    BOTH = 3


class SeqMidiOutput(IntEnum):
    OFF = 0
    DIN = 1
    USB = 2
    BOTH = 3


class ArpMidiOutput(IntEnum):
    OFF = 0
    DIN = 1
    USB = 2
    BOTH = 3


# Parameter names, SysEx command templates and value ranges / enums

PARAMS = {
    "device-id": ("00 {value:02X}", (0, 127)),
    "midi-channel": ("0E 01 {tx:02X} {rx:02X}", [("tx", 0, 15), ("rx", 0, 16)]),
    "midi-in-transpose": ("0F {value:02X}", (-12, 12)),
    "velocity-info": (
        "10 {noteon:02X} {noteoff:02X} {curve:02X}",
        [("noteon", 0, 127), ("noteoff", 0, 127), ("curve", 0, 2)],
    ),
    "pitchbend-range": ("11 {value:02X} 00", (0, 24)),
    "key-priority": ("12 {value:02X}", KeyPriority),
    "multi-trigger": ("14 {value:02X} 00", (0, 1)),
    "modulation-curve": ("15 {value:02X}", ModulationCurve),
    "zero-cv-note": ("16 {value:02X}", (0, 127)),
    "midiclock-out": ("17 {value:02X}", ClockMidiOutput),
    "ext-clock-polarity": ("19 {value:02X}", ExtClockPolarity),
    "sync-clock-rate": ("1A {value:02X}", SyncClockRate),
    "sync-clock-source": ("1B {value:02X}", SyncClockSource),
    "modwheel-range": ("20 {value:02X}", (0, 4)),
    "modwheel-out": ("21 {value:02X}", MWMidiOutput),
    "pitchwheel-out": ("22 {value:02X}", PBMidiOutput),
    "keyboard-out": ("23 {value:02X}", KbdMidiOutput),
    "aftertouch-out": ("24 {value:02X}", ATMidiOutput),
    "sequencer-out": ("25 {value:02X}", SeqMidiOutput),
    "arp-out": ("26 {value:02X}", ArpMidiOutput),
    "factory-reset": ("7D", None),
}
