"""Command line interface for Poly D Configurator."""

import argparse
#!/usr/bin/env python3

import logging
import textwrap
import sys
from enum import EnumMeta

from .api import ArgumentCountError, ParameterValueError, PolyDConfigurator
from .params import PARAMS

__all__ = ("main",)
log = logging.getLogger("polydconfig")

EPILOG = """\
Actions:

scan - detect whether a Poly D is connected to the selected MIDI port (and is reponsive)
set  - set synth parameter (example: 'set pitchbend-range 2')

Parameters:

{params}
"""


class ErrorRaisingArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        self.print_usage(sys.stderr)
        raise argparse.ArgumentTypeError(message)


def main(args=None):
    ap = ErrorRaisingArgumentParser(
        prog="polydconfig",
        description=__doc__.splitlines()[0],
        epilog=EPILOG.format(params="\n".join(textwrap.wrap(", ".join(sorted(PARAMS)), width=80))),
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    ap.add_argument(
        "--dry-run", action="store_true", help="Don't send any MIDI, only log what would be sent"
    )
    ap.add_argument("-v", "--debug", action="store_true", help="Enable debug logging")
    ap.add_argument(
        "-d",
        "--device-id",
        type=int,
        default=0,
        help="SysEx device ID (1..127, default: %(default)s == address all devices)",
    )
    ap.add_argument(
        "-i", "--interactive", action="store_true", help="Select MIDI ports interactively"
    )
    ap.add_argument(
        "-p", "--port", default="POLY D", help="MIDI input / output port name (default: %(default)s s)"
    )
    ap.add_argument(
        "-t",
        "--timeout",
        default=3.0,
        type=float,
        help="Timeout for scan response (default: %(default).2f)",
    )
    ap.add_argument(
        "action",
        default="scan",
        help="Script action (default: %(default)s)",
    )
    ap.add_argument("args", nargs="*", help="Script action arguments (optional)")

    logging.basicConfig(level=logging.INFO, format="%(levelname)s - %(message)s")

    try:
        args = ap.parse_args(args)

    except (argparse.ArgumentError, argparse.ArgumentTypeError) as exc:
        log.error(str(exc))
    else:
        if args.dry_run:
            args.debug = True

        log.setLevel(level=logging.DEBUG if args.debug else logging.INFO)

        if args.action == "set":
            if args.args:
                name, *values = args.args

                if name in PARAMS:
                    polyd = PolyDConfigurator(
                        args.port if not args.interactive else None,
                        device_id=args.device_id,
                        dry_run=args.dry_run,
                        interactive=args.interactive,
                    )

                    try:
                        polyd.set_param(name, *values)
                    except (ArgumentCountError, ParameterValueError) as exc:
                        (log.exception if args.debug else log.error)(str(exc))
                        p_range = PARAMS[name][1]

                        if isinstance(p_range, list):
                            log.info(
                                "Supported parameter value ranges: {}".format(
                                    ", ".join("{}: {}..{}".format(*rng) for rng in p_range)
                                )
                            )
                        elif isinstance(p_range, tuple):
                            log.info("Supported parameter value range: {}..{}".format(*p_range))
                        elif isinstance(p_range, EnumMeta):
                            log.info(
                                "Supported parameter values: {}".format(
                                    ", ".join(e.name for e in p_range)
                                )
                            )
                    except (KeyboardInterrupt, EOFError):
                        return
                    else:
                        return
                    finally:
                        polyd.close()
                else:
                    log.error(f"Unsupported parameter '{name}'.")
            else:
                log.error("Action 'set' requires arguments: <parameter name> [<value>, ...]")
        elif args.action == "scan":
            try:
                polyd = PolyDConfigurator(
                    args.port if not args.interactive else None,
                    device_id=args.device_id,
                    dry_run=args.dry_run,
                    interactive=args.interactive,
                )
            except (KeyboardInterrupt, EOFError):
                return
            else:
                try:
                    info = polyd.device_inquiry(args.device_id, args.timeout)
                except IOError as exc:
                    (log.exception if args.debug else log.error)(str(exc))
                    sys.exit(1)
                else:
                    print(
                        "Manufacturer ID: {0:02X}h {1:02X}h {2:02X}h ({0} {1} {2})".format(
                            *info["manufacturer_id"]
                        )
                    )
                    print(
                        "Device family ID: {0:02X}h {1:02X}h ({0} {1})".format(
                            *info["device_family_id"]
                        )
                    )
                    print("Model ID: {0:02X}h ({0})".format(info["model_id"]))
                    print("Firmware version: {}.{}.{}".format(*info["version"]))
                    return
                finally:
                    polyd.close()
        else:
            log.error(f"Unsupported action '{args.action}'.")

    log.info("Use option '-h' or '--help' for command line help.")
    sys.exit(1)


if __name__ == "__main__":
    main()
