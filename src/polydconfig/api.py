#!/usr/bin/env python3
"""Configure a Behringer PolyD synthesizer via MIDI."""

import argparse
import logging
from enum import EnumMeta

import rtmidi
from rtmidi.midiutil import open_midiinput, open_midioutput

from .midiinputhandler import MidiInputHandler
from .params import PARAMS

__all__ = ("ArgumentCountError", "ParameterValueError", "PolyDConfigurator")
log = logging.getLogger("polydconfig")

SYSEX_HEADER = "F0 00 20 32 00 01 0C {device_id:02X} "


# Helper functions


def clamp(val, minval, maxval):
    return max(minval, min(maxval, val))


def norm(val, minval, maxval):
    return clamp(val, minval, maxval) - minval


# Custom exceptions


class ArgumentCountError(ValueError):
    """Raised when the wrong number of arguments for a synth parameter is passed."""


class ParameterValueError(ValueError):
    """Raised when the an invalid value for a synth parameter is passed."""


# Primary API class


class PolyDConfigurator:
    def __init__(
        self, device_name=None, device_id=0x00, connect=False, interactive=False, dry_run=False
    ):
        self.device_name = device_name
        self.device_id = device_id
        self.interactive = interactive
        self.dry_run = dry_run
        self._midiout = None
        self._midiin = None

        if connect:
            self.connect_midiout(interactive=interactive)

    def connect_midiout(self, interactive=None):
        if interactive is None:
            interactive = self.interactive

        self._midiout, name = open_midioutput(
            port=self.device_name,
            client_name="Poly D Configurator",
            interactive=interactive,
        )
        log.debug(f"Connected to MIDI output {name}.")

    def connect_midiin(self, interactive=None):
        if interactive is None:
            interactive = self.interactive

        self._midiin, name = open_midiinput(
            port=self.device_name,
            client_name="Poly D Configurator",
            interactive=interactive,
        )
        log.debug(f"Connected to MIDI input {name}.")
        self._inputhandler = MidiInputHandler(self._midiin, name)

    def close(self):
        if self._midiin:
            self._midiin.close_port()
            self._midiin.delete()
            self._midiin = None
            self._inputhandler = None

        if self._midiout:
            self._midiout.close_port()
            self._midiout.delete()
            self._midiout = None

    def send_sysex_command(self, cmd, **kwargs):
        kwargs.setdefault("device_id", self.device_id & 0x7F)
        cmd = (SYSEX_HEADER + cmd + " F7").format(**kwargs)

        if not self.dry_run and not self._midiout:
            if self.device_name and not self.interactive:
                log.debug("Trying to open MIDI output port '%s'...", self.device_name)
            else:
                log.debug("Prompting for MIDI output port...")

            self.connect_midiout()

        self._send(bytes.fromhex(cmd.replace(" ", "")))

    def device_inquiry(self, device_id=0x0, timeout=None):
        if device_id is None:
            device_id = self.device_id

        if not self._midiin:
            self.connect_midiin()

        self._inputhandler.flush()
        self.send_sysex_command("00")
        return self._inputhandler.expect(
            [
                0xF0,
                ("manufacturer_id", 3),
                ("device_family_id", 2),
                "model_id",
                "device_id",  # not present repsonses from other Behringer synth modules
                ("version", 3),
                0xF7,
            ]
        )

    def set_param(self, name, *values):
        cmd, p_range = PARAMS[name]

        if isinstance(p_range, list):
            if len(values) != len(p_range):
                raise ArgumentCountError(
                    "Parameter '{}' expects {} value arguments ({}), {} given.".format(
                        name, len(p_range), ", ".join(p for p, _, _ in p_range), len(values)
                    )
                )

            kwargs = {}
            for i, (pname, minval, maxval) in enumerate(p_range):
                try:
                    value = int(values[i])
                except (ValueError, TypeError):
                    raise ParameterValueError(
                        f"Value argument '{pname}' for parameter '{name}' must be an integer."
                    )

                if value < minval or value > maxval:
                    raise ParameterValueError(
                        f"Value argument '{pname}' for parameter '{name}' out of range."
                    )

                kwargs[pname] = norm(value, minval, maxval)
        elif isinstance(p_range, EnumMeta):
            if len(values) != 1:
                raise ArgumentCountError(
                    f"Parameter '{name}' expects one value argument, {len(values)} given."
                )

            try:
                value = int(values[0])
                value = p_range(value)
            except (ValueError, TypeError):
                try:
                    value = p_range[values[0].upper()]
                except KeyError:
                    raise ParameterValueError(
                        f"Value for parameter '{name}' must be a member of {p_range!r}."
                    )

            kwargs = {"value": value}
        elif isinstance(p_range, tuple):
            if len(values) != 1:
                raise ArgumentCountError(
                    f"Parameter '{name}' expects one value argument, {len(values)} given."
                )

            try:
                value = int(values[0])
            except (ValueError, TypeError):
                raise ParameterValueError(f"Value for parameter '{name}' must be an integer.")

            if value < p_range[0] or value > p_range[1]:
                raise ParameterValueError(f"Value argument for parameter '{name}' out of range.")

            kwargs = {"value": norm(value, p_range[0], p_range[1])}
        elif p_range is None:
            kwargs = {}

        if kwargs:
            log.debug(
                "Setting parameter '{}' to {}.".format(
                    name, ",".join((f"{k}={v}" for k, v in kwargs.items()))
                )
            )
        else:
            log.debug("Sending command '{}'.".format(name))

        self.send_sysex_command(cmd, **kwargs)

    # ---------------- Internal methods: ----------------------------------

    def _send(self, msg):
        log.debug("SEND: %s", " ".join(f"{x:02X}" for x in msg))

        if self._midiout and not self.dry_run:
            self._midiout.send_message(msg)
        else:
            log.warning("MIDI output not opened.")
