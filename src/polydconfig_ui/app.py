#!/bin/env python3

import logging
import os
import sys
from pathlib import Path

from PySide6.QtCore import QSettings, Signal, Slot
from PySide6.QtGui import QGuiApplication
from PySide6.QtQml import QQmlApplicationEngine
from PySide6.QtCore import QObject, QUrl

from polydconfig.api import ArgumentCountError, ParameterValueError, PolyDConfigurator
from polydconfig.params import KeyPriority, PARAMS


MAIN_QML = "main.qml"
PROG = "polydconfig-ui"
log = logging.getLogger(PROG)


class PolyDConfiguratorBackend(QObject):
    def __init__(self, device_name="POLY D", device_id=0x00):
        QObject.__init__(self)

        self.api = PolyDConfigurator(device_name, device_id=device_id, connect=True)

    @Slot(int, int)
    def set_midi_channels(self, tx, rx):
        self.api.set_param("midi-channel", tx, rx)

    @Slot(int)
    def set_pitchbend_range(self, value):
        self.api.set_param("pitchbend-range", value)

    @Slot(int)
    def set_midi_in_transpose(self, value):
        self.api.set_param("midi-in-transpose", value)

    @Slot(str)
    def set_key_priority(self, value):
        self.api.set_param("key-priority", KeyPriority[value.upper()])

    @Slot(int, int, int)
    def set_velocity_info(self, noteon, noteoff, curve):
        self.api.set_param("velocity-info", noteon, noteoff, curve)

    @Slot(bool)
    def set_multi_trigger(self, value):
        self.api.set_param("multi-trigger", 1 if value else 0)

    @Slot(int)
    def set_midiclock_out(self, value):
        self.api.set_param("midiclock-out", value)

    @Slot(int)
    def set_sync_clock_source(self, value):
        self.api.set_param("sync-clock-source", value)

    @Slot(int)
    def set_sync_clock_rate(self, value):
        self.api.set_param("sync-clock-rate", value)

    @Slot(int)
    def set_ext_clock_polarity(self, value):
        self.api.set_param("ext-clock-polarity", value)

    @Slot(int)
    def set_modwheel_range(self, value):
        self.api.set_param("modwheel-range", value)

    @Slot(int)
    def set_modwheel_out(self, value):
        self.api.set_param("modwheel-out", value)

    @Slot(int)
    def set_pitchwheel_out(self, value):
        self.api.set_param("pitchwheel-out", value)

    @Slot(int)
    def set_keyboard_out(self, value):
        self.api.set_param("keyboard-out", value)

    @Slot(int)
    def set_aftertouch_out(self, value):
        self.api.set_param("aftertouch-out", value)

    @Slot(int)
    def set_sequencer_out(self, value):
        self.api.set_param("sequencer-out", value)

    @Slot(int)
    def set_arp_out(self, value):
        self.api.set_param("arp-out", value)

    @Slot()
    def factory_reset(self):
        self.api.set_param("factory-reset")


class PolyDConfiguratorUIApp(QGuiApplication):
    """The main application object with the central event handling."""

    name = "Poly D Configurator"

    def __init__(self, args=None):
        if args is None:
            args = sys.argv

        super().__init__(args)
        self.engine = QQmlApplicationEngine()

        # Set up settings
        QSettings.setDefaultFormat(QSettings.IniFormat)
        self.config = QSettings()
        #self.config.setIniCodec('UTF-8')

        self.debug = True if '-v' in args[1:] else self.config.value('application/debug', False)
        logging.basicConfig(level=logging.DEBUG if self.debug else logging.INFO,
                    format='%(levelname)s - %(message)s')

        # Set App Extra Info
        self.setOrganizationName("chrisarndt.de")
        self.setOrganizationDomain("chrisarndt.de")

        # ~ # Set Icon
        # ~ app.setWindowIcon(QIcon("images/icon.ico"))

        # Get & attach backend to context
        self.backend = PolyDConfiguratorBackend()
        self.engine.rootContext().setContextProperty("backend", self.backend)

        # Load QML
        qml_path = os.fspath(Path(__file__).resolve().parent / MAIN_QML)

        if not os.path.exists(qml_path):
            qml_path = "qrc:/" + MAIN_QML

        self.engine.load(QUrl(qml_path))

        if not self.engine.rootObjects():
            raise IOError(f"Could not load main QML file '{qml_path}'.")


def main(args=None):
    try:
        app = PolyDConfiguratorUIApp(args)
    except IOError as exc:
        sys.exit(str(exc))
    else:
        sys.exit(app.exec())


if __name__ == "__main__":
    main()
