import QtQuick 2.3
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts

Page {
    implicitWidth: 600

    title: qsTr("Global Settings")

    Rectangle {
        id: background_settings
        anchors.fill: parent

        ScrollView {
            anchors.fill: parent

            GridLayout {
                id: config_form
                columns: 2
                anchors.fill: parent
                anchors.margins: 10

                function set_midi_channels() {
                    backend.set_midi_channels(midi_out_channel.currentIndex, midi_in_channel.currentIndex);
                }

                function set_velocity_info() {
                    var noteon = 0;

                    if (noteon_velocity_fixed.checked) {
                        noteon = noteon_velocity_value.value;
                    }

                    var noteoff = 0;
                    if (noteoff_velocity_fixed.checked) {
                        noteoff = noteoff_velocity_value.value;
                    }

                    backend.set_velocity_info(noteon, noteoff, velocity_curve.currentIndex);
                }

                Label {
                    text: qsTr("MIDI IN Channel:")
                    Layout.alignment: Qt.AlignRight
                }

                ComboBox {
                    id: midi_in_channel
                    onActivated: config_form.set_midi_channels()
                    currentIndex: 0
                    model: [
                        "Ch. 1",
                        "Ch. 2",
                        "Ch. 3",
                        "Ch. 4",
                        "Ch. 5",
                        "Ch. 6",
                        "Ch. 7",
                        "Ch. 8",
                        "Ch. 9",
                        "Ch. 10",
                        "Ch. 11",
                        "Ch. 12",
                        "Ch. 13",
                        "Ch. 14",
                        "Ch. 15",
                        "Ch. 16",
                        "Any",
                    ]
                }

                Label {
                    text: qsTr("MIDI OUT Channel:")
                    Layout.alignment: Qt.AlignRight
                }

                ComboBox {
                    id: midi_out_channel
                    onActivated: config_form.set_midi_channels()
                    currentIndex: 0
                    model: [
                        "Ch. 1",
                        "Ch. 2",
                        "Ch. 3",
                        "Ch. 4",
                        "Ch. 5",
                        "Ch. 6",
                        "Ch. 7",
                        "Ch. 8",
                        "Ch. 9",
                        "Ch. 10",
                        "Ch. 11",
                        "Ch. 12",
                        "Ch. 13",
                        "Ch. 14",
                        "Ch. 15",
                        "Ch. 16",
                    ]
                }

                Label {
                    text: qsTr("MIDI In Transpose:")
                    Layout.alignment: Qt.AlignRight
                }

                RowLayout {
                    SpinBox {
                        id: midi_in_transpose
                        onValueModified: backend.set_midi_in_transpose(value)
                        editable: true
                        wheelEnabled: true
                        value: 0
                        from: -12
                        to: +12
                    }
                    Label {
                        text: qsTr("semitones")
                    }
                }

                Label {
                    text: qsTr("Pitch Bend Range:")
                    Layout.alignment: Qt.AlignRight
                }

                RowLayout {
                    SpinBox {
                        id: pitchbend_range
                        onValueModified: backend.set_pitchbend_range(value)
                        editable: true
                        wheelEnabled: true
                        value: 12
                        from: 0
                        to: 24
                    }

                    Label {
                        text: qsTr("semitones")
                    }
                }

                Label {
                    text: qsTr("Key Priority:")
                    Layout.alignment: Qt.AlignRight
                }

                RowLayout {
                    RadioButton {
                        text: qsTr("Low")
                        onClicked: backend.set_key_priority(text)
                    }
                    RadioButton {
                        text: qsTr("High")
                        onClicked: backend.set_key_priority(text)
                        checked: true
                    }
                    RadioButton {
                        text: qsTr("Last")
                        onClicked: backend.set_key_priority(text)
                    }
                }



                Label {
                    text: qsTr("Multi Trigger:")
                    Layout.alignment: Qt.AlignRight
                }

                RowLayout {
                    RadioButton {
                        text: qsTr("Off")
                        onClicked: backend.set_multi_trigger(false)
                    }
                    RadioButton {
                        text: qsTr("On")
                        checked: true
                        onClicked: backend.set_multi_trigger(true)
                    }
                }

                Label {
                    text: qsTr("Velocity Note-On:")
                    Layout.alignment: Qt.AlignRight
                }

                RowLayout {
                    RadioButton {
                        id: noteon_velocity_dynamic
                        onClicked: config_form.set_velocity_info()
                        text: qsTr("Dynamic")
                        checked: true
                    }
                    RadioButton {
                        id: noteon_velocity_fixed
                        onClicked: config_form.set_velocity_info()
                        text: qsTr("Fixed")
                    }
                    SpinBox {
                        id: noteon_velocity_value
                        enabled: noteon_velocity_fixed.checked
                        onValueModified: config_form.set_velocity_info()
                        editable: true
                        wheelEnabled: true
                        value: 127
                        from: 1
                        to: 127
                    }
                }

                Label {
                    text: qsTr("Velocity Note-Off:")
                    Layout.alignment: Qt.AlignRight
                }

                RowLayout {
                    RadioButton {
                        id: noteoff_velocity_dynamic
                        onClicked: config_form.set_velocity_info()
                        text: qsTr("Dynamic")
                        checked: true
                    }
                    RadioButton {
                        id: noteoff_velocity_fixed
                        onClicked: config_form.set_velocity_info()
                        text: qsTr("Fixed")
                    }
                    SpinBox {
                        id: noteoff_velocity_value
                        enabled: noteoff_velocity_fixed.checked
                        onValueModified: config_form.set_velocity_info()
                        editable: true
                        wheelEnabled: true
                        value: 127
                        from: 1
                        to: 127
                    }
                }

                Label {
                    text: qsTr("Velocity Curve:")
                    Layout.alignment: Qt.AlignRight
                }

                ComboBox {
                    id: velocity_curve
                    onActivated: config_form.set_velocity_info()
                    currentIndex: 1
                    model: [
                        "Soft",
                        "Medium",
                        "Hard"
                    ]
                }

                Label {
                    text: qsTr("MIDI Clock Out:")
                    Layout.alignment: Qt.AlignRight
                }

                ComboBox {
                    id: midiclock_out
                    onActivated: backend.set_midiclock_out(currentIndex)
                    currentIndex: 3
                    model: [
                        "Off",
                        "DIN-MIDI",
                        "USB-MIDI",
                        "Both"
                    ]
                }

                Label {
                    text: qsTr("Sync Clock Source:")
                    Layout.alignment: Qt.AlignRight
                }

                ComboBox {
                    id: sync_clock_source
                    onActivated: backend.set_sync_clock_source(currentIndex)
                    currentIndex: 1
                    model: [
                        "Internal",
                        "DIN-MIDI",
                        "USB-MIDI",
                        "Trigger IN"
                    ]
                }

                Label {
                    text: qsTr("Sync Clock Rate:")
                    Layout.alignment: Qt.AlignRight
                }

                RowLayout {
                    ComboBox {
                        id: sync_clock_rate
                        onActivated: backend.set_sync_clock_rate(currentIndex)
                        currentIndex: 2
                        model: [
                            "1",
                            "2",
                            "24",
                            "48"
                        ]
                    }

                    Label {
                        text: qsTr("PPQN")
                    }
                }

                Label {
                    text: qsTr("Ext. Clock Polarity:")
                    Layout.alignment: Qt.AlignRight
                }

                ComboBox {
                    id: ext_clock_polarity
                    onActivated: backend.set_ext_clock_polarity(currentIndex)
                    currentIndex: 1
                    model: [
                        "Falling",
                        "Rising",
                    ]
                }

                Label {
                    text: qsTr("Modwheel Range:")
                    Layout.alignment: Qt.AlignRight
                }

                ComboBox {
                    id: modwheel_range
                    onActivated: backend.set_modwheel_range(currentIndex)
                    currentIndex: 2
                    model: [
                        "0",
                        "1",
                        "2",
                        "3",
                        "4",
                    ]
                }

                Label {
                    text: qsTr("Modwheel Out:")
                    Layout.alignment: Qt.AlignRight
                }

                ComboBox {
                    id: modwheel_out
                    onActivated: backend.set_modwheel_out(currentIndex)
                    currentIndex: 3
                    model: [
                        "Off",
                        "DIN-MIDI",
                        "USB-MIDI",
                        "Both"
                    ]
                }

                Label {
                    text: qsTr("Pitchwheel Out:")
                    Layout.alignment: Qt.AlignRight
                }

                ComboBox {
                    id: pitchwheel_out
                    onActivated: backend.set_pitchwheel_out(currentIndex)
                    currentIndex: 3
                    model: [
                        "Off",
                        "DIN-MIDI",
                        "USB-MIDI",
                        "Both"
                    ]
                }

                Label {
                    text: qsTr("Keyboard Out:")
                    Layout.alignment: Qt.AlignRight
                }

                ComboBox {
                    id: keyboard_out
                    onActivated: backend.set_keyboard_out(currentIndex)
                    currentIndex: 3
                    model: [
                        "Off",
                        "DIN-MIDI",
                        "USB-MIDI",
                        "Both"
                    ]
                }

                Label {
                    text: qsTr("Aftertouch Out:")
                    Layout.alignment: Qt.AlignRight
                }

                ComboBox {
                    id: aftertouch_out
                    onActivated: backend.set_aftertouch_out(currentIndex)
                    currentIndex: 3
                    model: [
                        "Off",
                        "DIN-MIDI",
                        "USB-MIDI",
                        "Both"
                    ]
                }

                Label {
                    text: qsTr("Sequencer Out:")
                    Layout.alignment: Qt.AlignRight
                }

                ComboBox {
                    id: sequencer_out
                    onActivated: backend.set_sequencer_out(currentIndex)
                    currentIndex: 3
                    model: [
                        "Off",
                        "DIN-MIDI",
                        "USB-MIDI",
                        "Both"
                    ]
                }

                Label {
                    text: qsTr("Arpeggiator Out:")
                    Layout.alignment: Qt.AlignRight
                }

                ComboBox {
                    id: arp_out
                    onActivated: backend.set_arp_out(currentIndex)
                    currentIndex: 3
                    model: [
                        "Off",
                        "DIN-MIDI",
                        "USB-MIDI",
                        "Both"
                    ]
                }
            }
        }
    }
}
