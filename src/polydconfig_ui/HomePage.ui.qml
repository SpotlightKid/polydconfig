import QtQuick 2.3
import QtQuick.Controls.Material 2.12

Page {
    implicitWidth: 600

    title: qsTr("Home")

    Rectangle {
        id: rectangle
        anchors.fill: parent
        //color: "#ccc"

        Label {
            anchors.centerIn: parent
            color: "black"
            font.pointSize: 24
            x: 300
            y: 200
            text: qsTr("You are on the Home page.")
        }
    }
}
