import QtQuick 2.3
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.11

ApplicationWindow {
    id: window
    width: 800
    height: 600
    visible: true
    title: qsTr("Poly D Configurator")

    Material.theme: Material.Light
    Material.accent: Material.DeepOrange

    header: TabBar {
        id: tabbar
        width: parent.width
        currentIndex: 1

        TabButton {
            text: qsTr("Home")
            implicitHeight: 40
        }

        TabButton {
            text: qsTr("Configuration")
            implicitHeight: 40
        }

        TabButton {
            text: qsTr("About")
            implicitHeight: 40
        }
    }

    StackLayout {
        id: stackview
        anchors.fill: parent
        currentIndex: tabbar.currentIndex

        HomePage {}

        ConfigFormPage {}

        AboutPage {}
    }
}
