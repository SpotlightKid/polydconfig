# Poly D Configurator

A [Behringer Poly D] synthesizer configuration tool


## Installation

Using [pipx]:

```
pipx install git+https://gitlab.com/SpotlightKid/polydconfig
```

or using plain [pip]:

```
python -m pip install --user git+https://gitlab.com/SpotlightKid/polydconfig
```


## Usage

```
usage: polydconfig [-h] [--dry-run] [-v] [-d DEVICE_ID] [-i] [-p PORT] [-t TIMEOUT] action [args ...]

Command line interface for Poly D Configurator.

positional arguments:
  action                Script action (default: scan)
  args                  Script action arguments (optional)

options:
  -h, --help            show this help message and exit
  --dry-run             Don't send any MIDI, only log what would be sent
  -v, --debug           Enable debug logging
  -d DEVICE_ID, --device-id DEVICE_ID
                        SysEx device ID (1..127, default: 0 == address all devices)
  -i, --interactive     Select MIDI ports interactively
  -p PORT, --port PORT  MIDI output port name (default: POLY D)
  -t TIMEOUT, --timeout TIMEOUT
                        Timeout for scan response (default: 3.00 s)

Actions:

scan - detect whether a Poly D is connected to the selected MIDI port (and is reponsive)
set  - set synth parameter (example: 'set pitchbend-range 2')

Parameters:

aftertouch-out, arp-out, device-id, ext-clock-polarity, factory-reset, key-
priority, keyboard-out, midi-channel, midi-in-transpose, midiclock-out,
modulation-curve, modwheel-out, modwheel-range, multi-trigger, pitchbend-range,
pitchwheel-out, sequencer-out, sync-clock-rate, sync-clock-source, velocity-
info, zero-cv-note
```


[behringer poly d]: https://www.behringer.com/product.html?modelCode=P0D9J
[pip]: https://pip.pypa.io/en/stable/installation/
[pipx]: https://pypa.github.io/pipx/
